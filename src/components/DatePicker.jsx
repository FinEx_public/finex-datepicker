import React, { PureComponent } from 'react'
import { TextMask, InputAdapter } from 'react-text-mask-hoc'
import onClickOutside from "react-onclickoutside";
import PropTypes from 'prop-types'
import moment from 'moment'
import CalendarIcon from './misc/CalendarIcon.jsx'
import styled, { ThemeProvider } from 'styled-components'

import Calendar from './calendar/index.jsx'
// #B9E692

const DatePickerBlock = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`
const DatePickerBlockBody = styled.div`
  width: 165px;
  position: relative;
`
const InputIcon = styled.div`
  position: absolute;
  top: 5px;
  right: 14px;
`
const CalendarContainer = styled.div`
  visibility: ${props => props.status?'visible':'hidden'};
  transition: opacity 300ms linear;
  z-index: 2;
`
const ErrorText = styled.span`
  color: #F42300;
`
const DisabledText = styled.span`
  color: rgba(0, 0, 0, 0.5);
`
class DatePicker extends PureComponent {
  state = {
    showCalendar: false,
    input: null,
    calendData: null,
    theme: {
      primary: '#FED954',
      weekend: '#ee0000',
      monthBg: '#EDEDED',
      yearsBg: '#363636',
      daysBg: '#fff',
      yearActiveFontColor: '#000',
      yearFontColor: '#fff',
      monthActiveFontColor: '#000',
      monthFontColor: '#000',
      dayActiveFontColor: '#000',
      dayFontColor: '#000',
      monthNameFontColor: '#AAAAAA',
      yearHoverColor: '#000',
      monthAndDayHoverColor: '#ccc',
    }
  }
  getInitVal(){
    const { val, start, end } = this.props
    const day = parseInt(val.slice(0,2))
    const month = parseInt(val.slice(3,5))
    const year = parseInt(val.slice(6,11))
    if( year > 999 ){
      if( year >= start.getFullYear() && 
        year <= end.getFullYear() && 
        month <= 12 && 
        day <= moment([year, month - 1, 1]).daysInMonth() 
      ) {
        this.setState({
          input: val,
          calendData: {
            day,
            month,
            year
          }
        })
      }
    }
  }
  componentDidUpdate(prevProps, prevState){
    const { val } = this.props
    if(prevProps.val!=val&&val){
      this.getInitVal()
    }
  }
  componentDidMount(){
    this.setState({ theme: Object.assign(this.state.theme,this.props.theme) })
    if(this.props.val){
      this.getInitVal()
    }

  }
  openCalendar = val => {
    this.setState({ showCalendar: true })
    if(this.props.val){
      this.getInitVal()
    }
  }
  calendarNewDate = val => {
    this.setState({ input: val })
  }
  handleClickOutside = evt => {
    this.setState({ showCalendar: false })
  }
  inputChange = (e, newState) => {
    let { start, end } = this.props

    const input = newState.value
    
    const day = parseInt(input.slice(0,2))
    const month = parseInt(input.slice(3,5))
    const year = parseInt(input.slice(6,11))
    if( year > 999 ){
      if( year >= start.getFullYear() && 
        year <= end.getFullYear() && 
        month <= 12 && 
        day <= moment([year, month - 1, 1]).daysInMonth() 
      ) {
        
        this.setState({
          input,
          calendData: {
            day,
            month,
            year
          }
        },this.props.action(input))
      }
    }
    
  }
  render(){

    const TextInputStyle = {
      width: '165px',
      height: '32px',
      fontSize: '15x',
      border: '1px solid #ccc',
      boxSizing: 'border-box',
      padding: '7px 10px',
      outline: 'none',
      transition: 'border .3s linear',
    }

    let { showCalendar, input, calendData, theme } = this.state
    let { start, end, title, error, val, disabled } = this.props
    let inputActiveStyle = showCalendar ? { border: '2px solid ' + theme.primary } : null

    return(
      // don't remove this class. It makes styles for this date picker scoped
      <ThemeProvider theme={ theme }>
        <DatePickerBlock>
          <DatePickerBlockBody>
            {
              !disabled?(
                <div>
                  <TextMask
                    Component={ InputAdapter }
                    mask={[/[0-3]/, /[0-9]/,'.', /[0-1]/, /[0-9]/, '.', /[1-2]/,  /[0-9]/,  /[0-9]/,  /[0-9]/]}
                    guide
                    showMask 
                    onChange={ this.inputChange }
                    onFocus={ this.openCalendar }
                    style={Object.assign(TextInputStyle, inputActiveStyle)}
                    value={ val }
                  />
                  <InputIcon>
                    <CalendarIcon color={ theme.primary } active={ showCalendar } />
                  </InputIcon>
                  <CalendarContainer status={ showCalendar }>
                    <Calendar theme={ theme } data={ calendData } change={ this.props.action } start={ start } end={ end } />
                  </CalendarContainer>
                  <ErrorText>{ error }</ErrorText>
                </div>
              ):(
                <DisabledText>{ val }</DisabledText> 
              )
            }
            
          </DatePickerBlockBody>
        </DatePickerBlock>
      </ThemeProvider>
    )
  }
}

const birthStart = moment().subtract(118, 'year')
const birthEnd = moment().subtract(18,'year')
DatePicker.propTypes = {
  start: PropTypes.object,
  end: PropTypes.object,
  theme: PropTypes.object
}
DatePicker.defaultProps = {
  start: birthStart.toDate(),
  end: birthEnd.toDate(),
  error: '',
  val: ''
  
}
export default onClickOutside(DatePicker);