import React, { PureComponent } from 'react'

export default class CalendarIcon extends PureComponent {
  
  render(){
    const { color, active } = this.props 
    return(
      <svg width="24px" height="24px" viewBox="0 0 24 24" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
        <title>icn/calendar</title>
        <desc>Created with Sketch.</desc>
        <defs>
            <path d="M9,6 L12,6 L12,9 L9,9 L9,6 Z M13,6 L16,6 L16,9 L13,9 L13,6 Z M17,6 L20,6 L20,9 L17,9 L17,6 Z M17,10 L20,10 L20,13 L17,13 L17,10 Z M13,10 L16,10 L16,13 L13,13 L13,10 Z M9,10 L12,10 L12,13 L9,13 L9,10 Z M5,10 L8,10 L8,13 L5,13 L5,10 Z M5,14 L8,14 L8,17 L5,17 L5,14 Z M9,14 L12,14 L12,17 L9,17 L9,14 Z M13,14 L16,14 L16,17 L13,17 L13,14 Z" id="path-1"></path>
        </defs>
        <g id="icn/calendar" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <mask id="mask-2" fill="#000000">
                <use xlinkHref="#path-1"></use>
            </mask>
            <use style={{transition: 'fill .3s linear'}} id="Mask" fill={ color && active ? color : "#000000" } xlinkHref="#path-1"></use>
            <g id="main" mask="url(#mask-2)" fill="#000000">
                <rect id="Rectangle" x="0" y="0" width="24" height="24"></rect>
            </g>
        </g>
      </svg>
    )
  }
}