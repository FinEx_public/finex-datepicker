import React, { Component } from 'react'
import moment from 'moment'
import styled from 'styled-components'
import months from './months.js'
import ItemBlock from './MonthAndYearItem.jsx'
import DayBlock from './DayBlock.jsx'
import pad from './functions/Pad.js'
import checkStateChanged from './functions/checkStateChanged.js'

const MonthName = styled.span`
  margin-top: 10px;
  margin-left: 10px;
  color: ${props=> props.fontColor};
  font-size: 13px;
  text-transform: uppercase;
  letter-spacing: 2px;
`
const ColBlock = styled.div`
  height: 100%;
  padding: 20px 0;
  box-sizing: border-box;
  @media (max-width: 600px){
    height: auto;
    width: 100%;
    padding: 0;
  }
  color: ${ props => props.fontColor };
  background-color: ${ props => props.background };
`
const YearsBlock = styled(ColBlock)`
  width: 70px;
`
const MonthsBlock = styled(ColBlock)`
  width: 100px;
`
const DaysBlock = styled(ColBlock)`
  flex: 1;
  box-sizing: border-box;
  padding: 18px 10px;
`
const SidemenuBlock = styled.div`
  overflow-y: auto;
  height: 100%;
  cursor: pointer;
  @media (max-width: 600px){
    overflow-x: auto;
    display: flex;
    width: 100%;
  }
`
const DaysCont = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  margin-top: 12px;
  @media (max-width: 600px){
    width: 225px;
    margin: 0 auto;
  }
`

const CalendarBlock = styled.div`
  position: absolute;
  z-index: 2;
  top: calc(100% + 8px);
  width: 425px;
  height: 272px;
  box-shadow: 0px 2px 10px 0px rgba(0,0,0,0.6);
  display: flex;
  @media (max-width: 600px){
    flex-direction: column;
    width: 285px;
    height: auto;
  }
`

export default class Calendar extends Component {
  yearsEnd = React.createRef()

  state={
    years: [],
    year: null,
    month: null,
    days: [],
    daysStart: null,
    date: {
      val: null
    }
  }
  
  componentDidUpdate( prevProps, prevState ){
    const { date, year, month } = this.state
    const { data, end, start } = this.props
    if(prevProps.end!=end || prevProps.start!=start) this.handleGetYears(this.props.start,this.props.end)

    if( checkStateChanged(prevState, this.state) ){
      const dateString =  pad(date.val) + '.' + pad(month) + '.' + year
      this.props.change(dateString)
    }
    if( data !== prevProps.data ){
      const inputYear = data.year
      const inputMonth = data.month
      const inputDay = data.day

      this.setState({ date: { val: inputDay }, month: inputMonth, year: inputYear }, this.handleGetDays)
    }
  }

  handleGetDays = () => {
    const { year, month, date } = this.state
    let dateMonth = moment([year, month - 1, 1])
    let result = []
    let first_iteration = true
    let startOfMonthDay = null
    console.log(dateMonth)
    while (dateMonth.month() === month - 1) {
      if(first_iteration) {
        startOfMonthDay = dateMonth.day()
        first_iteration = false
      }
      result.push({
        val: dateMonth.date(),
        isWeekend: dateMonth.day() === 6 || dateMonth.day() === 0
      })
      dateMonth.add(1,'day')
    }

    if(date&&result[result.length - 1]<date) this.setState({ date: null }) 

    this.setState({ days: result, daysStart: startOfMonthDay }, this.addEmptyDays)
  }

  addEmptyDays = () => {
    const { days, daysStart } = this.state
    if( daysStart === 0 ) {
      this.setState({ 
        days : [...Array(6)].fill('').concat(days) 
      })
    }
    else {
      this.setState({
        days: [...Array(daysStart - 1)].fill('').concat(days)
      })
    }
  }

  handleChangeDate = (val) => {
    this.setState({ date: val })
  }

  handleChangeYear = (val) => {
    this.state.month?
      this.setState({ year: val }, this.handleGetDays)
      :
      this.setState({ year: val, month: 1, date: { val: 1 } }, this.handleGetDays)
  }

  handleChangeMonth = (ind) => {
    const { years, year } = this.state

    if(!year) this.setState({ year: parseInt(years[0].format('YYYY')), date: { val: 1 } })

    this.setState({ month: ind + 1 }, this.handleGetDays)
  }

  handleGetYears = (start,end) => {
    var years = moment(end).diff(start, 'years')
    var yearsBetween = []
    for(var year = 0; year <= years; year++){
      let newYear = moment(start).add( year, 'years' )
      yearsBetween.push(newYear)
    }
    this.setState({ years: yearsBetween.reverse() })
  }

  componentDidMount(){
    this.handleGetYears(this.props.start,this.props.end)
  }

  render(){
    const { month, years, year, days, date } = this.state
    const { theme } = this.props
    return(
      <CalendarBlock>
        <YearsBlock fontColor={ theme.yearFontColor } background={ theme.yearsBg }>
          <SidemenuBlock>
            {
              years.map((item,ind)=>{
                  const itemYear = parseInt(item.format('YYYY'))
                  return(
                    <ItemBlock 
                      text={ itemYear } 
                      selected={ itemYear === year }
                      action={ () => this.handleChangeYear(itemYear) }  
                      hover={ theme.yearHoverColor }
                      key={ 'year' + ind }
                      dark 
                      theme={ theme }
                    />
                  )
                }
              )
            }
            <div ref={this.yearsEnd} />
          </SidemenuBlock>
        </YearsBlock>
        <MonthsBlock fontColor={ theme.monthFontColor } background={ theme.monthBg }>
          <SidemenuBlock>
            {
              months.map((item,ind)=>
                <ItemBlock 
                  action={ () => this.handleChangeMonth(ind) } 
                  text={ item.name } 
                  key={ 'month' + ind } 
                  selected={ ind === month - 1 }
                  theme={ theme }
                  hover={ theme.monthAndDayHoverColor }
                />
              )
            }
          </SidemenuBlock>
          
        </MonthsBlock>
        <DaysBlock fontColor={ theme.dayFontColor } background={ theme.daysBg }>
          { month &&
            <div>
              <MonthName fontColor={ theme.monthNameFontColor }>{ months[month - 1].name }</MonthName>
              <DaysCont>
                {
                  days.map((item,ind)=>
                    <DayBlock 
                      action={ () => this.handleChangeDate(item) } 
                      selected={ date.val === item.val } 
                      data={ item } 
                      key={'days' + ind} 
                      theme={ theme }
                    />
                  )
                }
              </DaysCont>
            </div>
          }
        </DaysBlock>
      </CalendarBlock>
    )
  }
}