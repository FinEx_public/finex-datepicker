export default function pad(prevState,newState) {

  const dateOld = prevState.date
  const yearOld = prevState.year
  const monthOld = prevState.month

  const { date, year, month } = newState

  if ( date && year && month ){
    if( date !== dateOld || year !== yearOld || month !== monthOld ) return true
  }
  return false

}