import React, { Component } from 'react'
import styled from 'styled-components'

const Item = styled.div`
  width: 100%;
  height: 35px;
  line-height: 35px;
  padding-left: 20px;
  box-sizing: border-box;
  transition: background-color .3s linear;
  font-size: 13px;
  user-select: none;
  @media (max-width: 600px){
    padding: 0 20px;
  }
  :hover{
    background-color: ${ props => !props.selected && props.hover };
  }
  color: ${ props =>  props.selected && ( (props.dark && props.theme.yearActiveFontColor) || props.theme.monthActiveFontColor ) };
  background-color: ${ props =>  props.selected && ((props.dark &&props.theme.monthBg) || props.theme.daysBg) };
`

export default class MonthAndYearItem extends Component {

  render(){
    const { selected, text, action, dark,  hover } = this.props
    

    return(
      <Item 
        dark={ dark } 
        hover={ hover }
        selected={ selected } 
        onClick={ action } 
        className='block' >
        <span> { text } </span>
      </Item>
    )
  }
}