import React, { Component } from 'react'
import styled from 'styled-components'

const DayBtn = styled.div`
  width: 32px;
  height: 32px;
  text-align: center;
  line-height: 32px;
  font-size: 15px;
  transition: background-color .3s linear;
  cursor: pointer;
  background-color: ${props => props.active && props.theme.primary };
  color: ${props => ( (props.isWeekend && !props.active) && props.theme.weekend  ) || ( props.active && props.theme.dayActiveFontColor ) };
  :hover{
    background-color: ${props => props.active ? props.theme.primary : !props.isEmpty && props.theme.monthAndDayHoverColor }
  }
  
`

export default class DayBlock extends Component {

  render(){
    const { data, selected, action } = this.props

    return(
      <DayBtn 
        onClick={ data !== '' ? action : undefined } 
        active={ selected }
        isWeekend={ data.isWeekend }
        isEmpty={ data === '' }
      >
        <span> { data.val } </span>
      </DayBtn>
    )
  }
}