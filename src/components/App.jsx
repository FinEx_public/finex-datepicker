import React, { Component } from 'react'
import DatePicker from './DatePicker.jsx'
function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}
export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      randomStart: randomDate(new Date(1900, 0, 1), new Date())
    };

    this.handleChangeDate = this.handleChangeDate.bind(this);
  }
  change(val){
    console.log(val)
  }
  handleChangeDate(){
    this.setState({val: '14.11.1991'})
  }
  render(){
    const { randomStart } = this.state

    return(
      <div>
        <DatePicker action={this.change} val='11.12.1983' start={ randomStart } end={ new Date() } />
        <button onClick={ this.handleChangeDate }>change Date</button>
      </div>
    )
  }
}
