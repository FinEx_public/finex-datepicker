import babel from 'rollup-plugin-babel'
import commonjs from 'rollup-plugin-commonjs'
import external from 'rollup-plugin-peer-deps-external'
import postcss from 'rollup-plugin-postcss'
import resolve from 'rollup-plugin-node-resolve'
import url from 'rollup-plugin-url'
import sass from 'rollup-plugin-sass'


export default {
  input: 'src/index.js',
  output: {
    file: 'dist/index.js',
    format: 'cjs',
  },
  plugins: [
    external(),
    postcss({
      modules: true
    }),
    url(),
    sass({
      // Default behaviour disable output
      output: false,
     
      // Write all styles to the bundle destination where .js is replaced by .css
      output: true,
     
      // Filename to write all styles
      output: 'bundle.css',
     
      // Callback that will be called ongenerate with two arguments:
      // - styles: the concatenated styles in order of imported
      // - styleNodes: an array of style objects:
      //  [
      //    { id: './style1.scss', content: 'body { color: red };' },
      //    { id: './style2.scss', content: 'body { color: green };' }
      //  ]
    }),
    babel({
      exclude: 'node_modules/**',
      plugins: [ 'external-helpers' ]
    }),
    resolve(),
    commonjs(),
  ]
}
