'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);
var styled = require('styled-components');
var styled__default = _interopDefault(styled);
var moment = _interopDefault(require('moment'));
var reactTextMaskHoc = require('react-text-mask-hoc');
var onClickOutside = _interopDefault(require('react-onclickoutside'));
var PropTypes = _interopDefault(require('prop-types'));

var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};

var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var taggedTemplateLiteral = function (strings, raw) {
  return Object.freeze(Object.defineProperties(strings, {
    raw: {
      value: Object.freeze(raw)
    }
  }));
};

var toConsumableArray = function (arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  } else {
    return Array.from(arr);
  }
};

var CalendarIcon = function (_PureComponent) {
  inherits(CalendarIcon, _PureComponent);

  function CalendarIcon() {
    classCallCheck(this, CalendarIcon);
    return possibleConstructorReturn(this, (CalendarIcon.__proto__ || Object.getPrototypeOf(CalendarIcon)).apply(this, arguments));
  }

  createClass(CalendarIcon, [{
    key: "render",
    value: function render() {
      var _props = this.props,
          color = _props.color,
          active = _props.active;

      return React__default.createElement(
        "svg",
        { width: "24px", height: "24px", viewBox: "0 0 24 24", version: "1.1", xmlns: "http://www.w3.org/2000/svg", xmlnsXlink: "http://www.w3.org/1999/xlink" },
        React__default.createElement(
          "title",
          null,
          "icn/calendar"
        ),
        React__default.createElement(
          "desc",
          null,
          "Created with Sketch."
        ),
        React__default.createElement(
          "defs",
          null,
          React__default.createElement("path", { d: "M9,6 L12,6 L12,9 L9,9 L9,6 Z M13,6 L16,6 L16,9 L13,9 L13,6 Z M17,6 L20,6 L20,9 L17,9 L17,6 Z M17,10 L20,10 L20,13 L17,13 L17,10 Z M13,10 L16,10 L16,13 L13,13 L13,10 Z M9,10 L12,10 L12,13 L9,13 L9,10 Z M5,10 L8,10 L8,13 L5,13 L5,10 Z M5,14 L8,14 L8,17 L5,17 L5,14 Z M9,14 L12,14 L12,17 L9,17 L9,14 Z M13,14 L16,14 L16,17 L13,17 L13,14 Z", id: "path-1" })
        ),
        React__default.createElement(
          "g",
          { id: "icn/calendar", stroke: "none", strokeWidth: "1", fill: "none", fillRule: "evenodd" },
          React__default.createElement(
            "mask",
            { id: "mask-2", fill: "#000000" },
            React__default.createElement("use", { xlinkHref: "#path-1" })
          ),
          React__default.createElement("use", { style: { transition: 'fill .3s linear' }, id: "Mask", fill: color && active ? color : "#000000", xlinkHref: "#path-1" }),
          React__default.createElement(
            "g",
            { id: "main", mask: "url(#mask-2)", fill: "#000000" },
            React__default.createElement("rect", { id: "Rectangle", x: "0", y: "0", width: "24", height: "24" })
          )
        )
      );
    }
  }]);
  return CalendarIcon;
}(React.PureComponent);

var months = [{
  name: 'Январь'
}, {
  name: 'Февраль'
}, {
  name: 'Март'
}, {
  name: 'Апрель'
}, {
  name: 'Май'
}, {
  name: 'Июнь'
}, {
  name: 'Июль'
}, {
  name: 'Август'
}, {
  name: 'Сентябрь'
}, {
  name: 'Октябрь'
}, {
  name: 'Ноябрь'
}, {
  name: 'Декабрь'
}];

var _templateObject = taggedTemplateLiteral(['\n  width: 100%;\n  height: 35px;\n  line-height: 35px;\n  padding-left: 20px;\n  box-sizing: border-box;\n  transition: background-color .3s linear;\n  font-size: 13px;\n  user-select: none;\n  @media (max-width: 600px){\n    padding: 0 20px;\n  }\n  :hover{\n    background-color: ', ';\n  }\n  color: ', ';\n  background-color: ', ';\n'], ['\n  width: 100%;\n  height: 35px;\n  line-height: 35px;\n  padding-left: 20px;\n  box-sizing: border-box;\n  transition: background-color .3s linear;\n  font-size: 13px;\n  user-select: none;\n  @media (max-width: 600px){\n    padding: 0 20px;\n  }\n  :hover{\n    background-color: ', ';\n  }\n  color: ', ';\n  background-color: ', ';\n']);

var Item = styled__default.div(_templateObject, function (props) {
  return !props.selected && props.hover;
}, function (props) {
  return props.selected && (props.dark && props.theme.yearActiveFontColor || props.theme.monthActiveFontColor);
}, function (props) {
  return props.selected && (props.dark && props.theme.monthBg || props.theme.daysBg);
});

var MonthAndYearItem = function (_Component) {
  inherits(MonthAndYearItem, _Component);

  function MonthAndYearItem() {
    classCallCheck(this, MonthAndYearItem);
    return possibleConstructorReturn(this, (MonthAndYearItem.__proto__ || Object.getPrototypeOf(MonthAndYearItem)).apply(this, arguments));
  }

  createClass(MonthAndYearItem, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          selected = _props.selected,
          text = _props.text,
          action = _props.action,
          dark = _props.dark,
          hover = _props.hover;


      return React__default.createElement(
        Item,
        {
          dark: dark,
          hover: hover,
          selected: selected,
          onClick: action,
          className: 'block' },
        React__default.createElement(
          'span',
          null,
          ' ',
          text,
          ' '
        )
      );
    }
  }]);
  return MonthAndYearItem;
}(React.Component);

var _templateObject$1 = taggedTemplateLiteral(['\n  width: 32px;\n  height: 32px;\n  text-align: center;\n  line-height: 32px;\n  font-size: 15px;\n  transition: background-color .3s linear;\n  cursor: pointer;\n  background-color: ', ';\n  color: ', ';\n  :hover{\n    background-color: ', '\n  }\n  \n'], ['\n  width: 32px;\n  height: 32px;\n  text-align: center;\n  line-height: 32px;\n  font-size: 15px;\n  transition: background-color .3s linear;\n  cursor: pointer;\n  background-color: ', ';\n  color: ', ';\n  :hover{\n    background-color: ', '\n  }\n  \n']);

var DayBtn = styled__default.div(_templateObject$1, function (props) {
  return props.active && props.theme.primary;
}, function (props) {
  return props.isWeekend && !props.active && props.theme.weekend || props.active && props.theme.dayActiveFontColor;
}, function (props) {
  return props.active ? props.theme.primary : !props.isEmpty && props.theme.monthAndDayHoverColor;
});

var DayBlock = function (_Component) {
  inherits(DayBlock, _Component);

  function DayBlock() {
    classCallCheck(this, DayBlock);
    return possibleConstructorReturn(this, (DayBlock.__proto__ || Object.getPrototypeOf(DayBlock)).apply(this, arguments));
  }

  createClass(DayBlock, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          data = _props.data,
          selected = _props.selected,
          action = _props.action;


      return React__default.createElement(
        DayBtn,
        {
          onClick: data !== '' ? action : undefined,
          active: selected,
          isWeekend: data.isWeekend,
          isEmpty: data === ''
        },
        React__default.createElement(
          'span',
          null,
          ' ',
          data.val,
          ' '
        )
      );
    }
  }]);
  return DayBlock;
}(React.Component);

function pad(d) {
  return d < 10 ? '0' + d.toString() : d.toString();
}

function pad$1(prevState, newState) {

  var dateOld = prevState.date;
  var yearOld = prevState.year;
  var monthOld = prevState.month;

  var date = newState.date,
      year = newState.year,
      month = newState.month;


  if (date && year && month) {
    if (date !== dateOld || year !== yearOld || month !== monthOld) return true;
  }
  return false;
}

var _templateObject$2 = taggedTemplateLiteral(['\n  margin-top: 10px;\n  margin-left: 10px;\n  color: ', ';\n  font-size: 13px;\n  text-transform: uppercase;\n  letter-spacing: 2px;\n'], ['\n  margin-top: 10px;\n  margin-left: 10px;\n  color: ', ';\n  font-size: 13px;\n  text-transform: uppercase;\n  letter-spacing: 2px;\n']),
    _templateObject2 = taggedTemplateLiteral(['\n  height: 100%;\n  padding: 20px 0;\n  box-sizing: border-box;\n  @media (max-width: 600px){\n    height: auto;\n    width: 100%;\n    padding: 0;\n  }\n  color: ', ';\n  background-color: ', ';\n'], ['\n  height: 100%;\n  padding: 20px 0;\n  box-sizing: border-box;\n  @media (max-width: 600px){\n    height: auto;\n    width: 100%;\n    padding: 0;\n  }\n  color: ', ';\n  background-color: ', ';\n']),
    _templateObject3 = taggedTemplateLiteral(['\n  width: 70px;\n'], ['\n  width: 70px;\n']),
    _templateObject4 = taggedTemplateLiteral(['\n  width: 100px;\n'], ['\n  width: 100px;\n']),
    _templateObject5 = taggedTemplateLiteral(['\n  flex: 1;\n  box-sizing: border-box;\n  padding: 18px 10px;\n'], ['\n  flex: 1;\n  box-sizing: border-box;\n  padding: 18px 10px;\n']),
    _templateObject6 = taggedTemplateLiteral(['\n  overflow-y: auto;\n  height: 100%;\n  cursor: pointer;\n  @media (max-width: 600px){\n    overflow-x: auto;\n    display: flex;\n    width: 100%;\n  }\n'], ['\n  overflow-y: auto;\n  height: 100%;\n  cursor: pointer;\n  @media (max-width: 600px){\n    overflow-x: auto;\n    display: flex;\n    width: 100%;\n  }\n']),
    _templateObject7 = taggedTemplateLiteral(['\n  display: flex;\n  flex-wrap: wrap;\n  width: 100%;\n  margin-top: 12px;\n  @media (max-width: 600px){\n    width: 225px;\n    margin: 0 auto;\n  }\n'], ['\n  display: flex;\n  flex-wrap: wrap;\n  width: 100%;\n  margin-top: 12px;\n  @media (max-width: 600px){\n    width: 225px;\n    margin: 0 auto;\n  }\n']),
    _templateObject8 = taggedTemplateLiteral(['\n  position: absolute;\n  z-index: 2;\n  top: calc(100% + 8px);\n  width: 425px;\n  height: 272px;\n  box-shadow: 0px 2px 10px 0px rgba(0,0,0,0.6);\n  display: flex;\n  @media (max-width: 600px){\n    flex-direction: column;\n    width: 285px;\n    height: auto;\n  }\n'], ['\n  position: absolute;\n  z-index: 2;\n  top: calc(100% + 8px);\n  width: 425px;\n  height: 272px;\n  box-shadow: 0px 2px 10px 0px rgba(0,0,0,0.6);\n  display: flex;\n  @media (max-width: 600px){\n    flex-direction: column;\n    width: 285px;\n    height: auto;\n  }\n']);

var MonthName = styled__default.span(_templateObject$2, function (props) {
  return props.fontColor;
});
var ColBlock = styled__default.div(_templateObject2, function (props) {
  return props.fontColor;
}, function (props) {
  return props.background;
});
var YearsBlock = styled__default(ColBlock)(_templateObject3);
var MonthsBlock = styled__default(ColBlock)(_templateObject4);
var DaysBlock = styled__default(ColBlock)(_templateObject5);
var SidemenuBlock = styled__default.div(_templateObject6);
var DaysCont = styled__default.div(_templateObject7);

var CalendarBlock = styled__default.div(_templateObject8);

var Calendar = function (_Component) {
  inherits(Calendar, _Component);

  function Calendar() {
    var _ref;

    var _temp, _this, _ret;

    classCallCheck(this, Calendar);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = Calendar.__proto__ || Object.getPrototypeOf(Calendar)).call.apply(_ref, [this].concat(args))), _this), _this.yearsEnd = React__default.createRef(), _this.state = {
      years: [],
      year: null,
      month: null,
      days: [],
      daysStart: null,
      date: {
        val: null
      }
    }, _this.handleGetDays = function () {
      var _this$state = _this.state,
          year = _this$state.year,
          month = _this$state.month,
          date = _this$state.date;

      var dateMonth = moment([year, month - 1, 1]);
      var result = [];
      var first_iteration = true;
      var startOfMonthDay = null;
      console.log(dateMonth);
      while (dateMonth.month() === month - 1) {
        if (first_iteration) {
          startOfMonthDay = dateMonth.day();
          first_iteration = false;
        }
        result.push({
          val: dateMonth.date(),
          isWeekend: dateMonth.day() === 6 || dateMonth.day() === 0
        });
        dateMonth.add(1, 'day');
      }

      if (date && result[result.length - 1] < date) _this.setState({ date: null });

      _this.setState({ days: result, daysStart: startOfMonthDay }, _this.addEmptyDays);
    }, _this.addEmptyDays = function () {
      var _this$state2 = _this.state,
          days = _this$state2.days,
          daysStart = _this$state2.daysStart;

      if (daysStart === 0) {
        _this.setState({
          days: [].concat(toConsumableArray(Array(6))).fill('').concat(days)
        });
      } else {
        _this.setState({
          days: [].concat(toConsumableArray(Array(daysStart - 1))).fill('').concat(days)
        });
      }
    }, _this.handleChangeDate = function (val) {
      _this.setState({ date: val });
    }, _this.handleChangeYear = function (val) {
      _this.state.month ? _this.setState({ year: val }, _this.handleGetDays) : _this.setState({ year: val, month: 1, date: { val: 1 } }, _this.handleGetDays);
    }, _this.handleChangeMonth = function (ind) {
      var _this$state3 = _this.state,
          years = _this$state3.years,
          year = _this$state3.year;


      if (!year) _this.setState({ year: parseInt(years[0].format('YYYY')), date: { val: 1 } });

      _this.setState({ month: ind + 1 }, _this.handleGetDays);
    }, _this.handleGetYears = function (start, end) {
      var years = moment(end).diff(start, 'years');
      var yearsBetween = [];
      for (var year = 0; year <= years; year++) {
        var newYear = moment(start).add(year, 'years');
        yearsBetween.push(newYear);
      }
      _this.setState({ years: yearsBetween.reverse() });
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(Calendar, [{
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      var _state = this.state,
          date = _state.date,
          year = _state.year,
          month = _state.month;
      var _props = this.props,
          data = _props.data,
          end = _props.end,
          start = _props.start;

      if (prevProps.end != end || prevProps.start != start) this.handleGetYears(this.props.start, this.props.end);

      if (pad$1(prevState, this.state)) {
        var dateString = pad(date.val) + '.' + pad(month) + '.' + year;
        this.props.change(dateString);
      }
      if (data !== prevProps.data) {
        var inputYear = data.year;
        var inputMonth = data.month;
        var inputDay = data.day;

        this.setState({ date: { val: inputDay }, month: inputMonth, year: inputYear }, this.handleGetDays);
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.handleGetYears(this.props.start, this.props.end);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _state2 = this.state,
          month = _state2.month,
          years = _state2.years,
          year = _state2.year,
          days = _state2.days,
          date = _state2.date;
      var theme = this.props.theme;

      return React__default.createElement(
        CalendarBlock,
        null,
        React__default.createElement(
          YearsBlock,
          { fontColor: theme.yearFontColor, background: theme.yearsBg },
          React__default.createElement(
            SidemenuBlock,
            null,
            years.map(function (item, ind) {
              var itemYear = parseInt(item.format('YYYY'));
              return React__default.createElement(MonthAndYearItem, {
                text: itemYear,
                selected: itemYear === year,
                action: function action() {
                  return _this2.handleChangeYear(itemYear);
                },
                hover: theme.yearHoverColor,
                key: 'year' + ind,
                dark: true,
                theme: theme
              });
            }),
            React__default.createElement('div', { ref: this.yearsEnd })
          )
        ),
        React__default.createElement(
          MonthsBlock,
          { fontColor: theme.monthFontColor, background: theme.monthBg },
          React__default.createElement(
            SidemenuBlock,
            null,
            months.map(function (item, ind) {
              return React__default.createElement(MonthAndYearItem, {
                action: function action() {
                  return _this2.handleChangeMonth(ind);
                },
                text: item.name,
                key: 'month' + ind,
                selected: ind === month - 1,
                theme: theme,
                hover: theme.monthAndDayHoverColor
              });
            })
          )
        ),
        React__default.createElement(
          DaysBlock,
          { fontColor: theme.dayFontColor, background: theme.daysBg },
          month && React__default.createElement(
            'div',
            null,
            React__default.createElement(
              MonthName,
              { fontColor: theme.monthNameFontColor },
              months[month - 1].name
            ),
            React__default.createElement(
              DaysCont,
              null,
              days.map(function (item, ind) {
                return React__default.createElement(DayBlock, {
                  action: function action() {
                    return _this2.handleChangeDate(item);
                  },
                  selected: date.val === item.val,
                  data: item,
                  key: 'days' + ind,
                  theme: theme
                });
              })
            )
          )
        )
      );
    }
  }]);
  return Calendar;
}(React.Component);

var _templateObject$3 = taggedTemplateLiteral(['\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n'], ['\n  display: flex;\n  justify-content: space-between;\n  align-items: center;\n']),
    _templateObject2$1 = taggedTemplateLiteral(['\n  width: 165px;\n  position: relative;\n'], ['\n  width: 165px;\n  position: relative;\n']),
    _templateObject3$1 = taggedTemplateLiteral(['\n  position: absolute;\n  top: 5px;\n  right: 14px;\n'], ['\n  position: absolute;\n  top: 5px;\n  right: 14px;\n']),
    _templateObject4$1 = taggedTemplateLiteral(['\n  visibility: ', ';\n  transition: opacity 300ms linear;\n  z-index: 2;\n'], ['\n  visibility: ', ';\n  transition: opacity 300ms linear;\n  z-index: 2;\n']),
    _templateObject5$1 = taggedTemplateLiteral(['\n  color: #F42300;\n'], ['\n  color: #F42300;\n']),
    _templateObject6$1 = taggedTemplateLiteral(['\n  color: rgba(0, 0, 0, 0.5);\n'], ['\n  color: rgba(0, 0, 0, 0.5);\n']);
// #B9E692

var DatePickerBlock = styled__default.div(_templateObject$3);
var DatePickerBlockBody = styled__default.div(_templateObject2$1);
var InputIcon = styled__default.div(_templateObject3$1);
var CalendarContainer = styled__default.div(_templateObject4$1, function (props) {
  return props.status ? 'visible' : 'hidden';
});
var ErrorText = styled__default.span(_templateObject5$1);
var DisabledText = styled__default.span(_templateObject6$1);

var DatePicker = function (_PureComponent) {
  inherits(DatePicker, _PureComponent);

  function DatePicker() {
    var _ref;

    var _temp, _this, _ret;

    classCallCheck(this, DatePicker);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = possibleConstructorReturn(this, (_ref = DatePicker.__proto__ || Object.getPrototypeOf(DatePicker)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      showCalendar: false,
      input: null,
      calendData: null,
      theme: {
        primary: '#FED954',
        weekend: '#ee0000',
        monthBg: '#EDEDED',
        yearsBg: '#363636',
        daysBg: '#fff',
        yearActiveFontColor: '#000',
        yearFontColor: '#fff',
        monthActiveFontColor: '#000',
        monthFontColor: '#000',
        dayActiveFontColor: '#000',
        dayFontColor: '#000',
        monthNameFontColor: '#AAAAAA',
        yearHoverColor: '#000',
        monthAndDayHoverColor: '#ccc'
      }
    }, _this.openCalendar = function (val) {
      _this.setState({ showCalendar: true });
      if (_this.props.val) {
        _this.getInitVal();
      }
    }, _this.calendarNewDate = function (val) {
      _this.setState({ input: val });
    }, _this.handleClickOutside = function (evt) {
      _this.setState({ showCalendar: false });
    }, _this.inputChange = function (e, newState) {
      var _this$props = _this.props,
          start = _this$props.start,
          end = _this$props.end;


      var input = newState.value;

      var day = parseInt(input.slice(0, 2));
      var month = parseInt(input.slice(3, 5));
      var year = parseInt(input.slice(6, 11));
      if (year > 999) {
        if (year >= start.getFullYear() && year <= end.getFullYear() && month <= 12 && day <= moment([year, month - 1, 1]).daysInMonth()) {

          _this.setState({
            input: input,
            calendData: {
              day: day,
              month: month,
              year: year
            }
          }, _this.props.action(input));
        }
      }
    }, _temp), possibleConstructorReturn(_this, _ret);
  }

  createClass(DatePicker, [{
    key: 'getInitVal',
    value: function getInitVal() {
      var _props = this.props,
          val = _props.val,
          start = _props.start,
          end = _props.end;

      var day = parseInt(val.slice(0, 2));
      var month = parseInt(val.slice(3, 5));
      var year = parseInt(val.slice(6, 11));
      if (year > 999) {
        if (year >= start.getFullYear() && year <= end.getFullYear() && month <= 12 && day <= moment([year, month - 1, 1]).daysInMonth()) {
          this.setState({
            input: val,
            calendData: {
              day: day,
              month: month,
              year: year
            }
          });
        }
      }
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate(prevProps, prevState) {
      var val = this.props.val;

      if (prevProps.val != val && val) {
        this.getInitVal();
      }
    }
  }, {
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.setState({ theme: Object.assign(this.state.theme, this.props.theme) });
      if (this.props.val) {
        this.getInitVal();
      }
    }
  }, {
    key: 'render',
    value: function render() {

      var TextInputStyle = {
        width: '165px',
        height: '32px',
        fontSize: '15x',
        border: '1px solid #ccc',
        boxSizing: 'border-box',
        padding: '7px 10px',
        outline: 'none',
        transition: 'border .3s linear'
      };

      var _state = this.state,
          showCalendar = _state.showCalendar,
          input = _state.input,
          calendData = _state.calendData,
          theme = _state.theme;
      var _props2 = this.props,
          start = _props2.start,
          end = _props2.end,
          title = _props2.title,
          error = _props2.error,
          val = _props2.val,
          disabled = _props2.disabled;

      var inputActiveStyle = showCalendar ? { border: '2px solid ' + theme.primary } : null;

      return (
        // don't remove this class. It makes styles for this date picker scoped
        React__default.createElement(
          styled.ThemeProvider,
          { theme: theme },
          React__default.createElement(
            DatePickerBlock,
            null,
            React__default.createElement(
              DatePickerBlockBody,
              null,
              !disabled ? React__default.createElement(
                'div',
                null,
                React__default.createElement(reactTextMaskHoc.TextMask, {
                  Component: reactTextMaskHoc.InputAdapter,
                  mask: [/[0-3]/, /[0-9]/, '.', /[0-1]/, /[0-9]/, '.', /[1-2]/, /[0-9]/, /[0-9]/, /[0-9]/],
                  guide: true,
                  showMask: true,
                  onChange: this.inputChange,
                  onFocus: this.openCalendar,
                  style: Object.assign(TextInputStyle, inputActiveStyle),
                  value: val
                }),
                React__default.createElement(
                  InputIcon,
                  null,
                  React__default.createElement(CalendarIcon, { color: theme.primary, active: showCalendar })
                ),
                React__default.createElement(
                  CalendarContainer,
                  { status: showCalendar },
                  React__default.createElement(Calendar, { theme: theme, data: calendData, change: this.props.action, start: start, end: end })
                ),
                React__default.createElement(
                  ErrorText,
                  null,
                  error
                )
              ) : React__default.createElement(
                DisabledText,
                null,
                val
              )
            )
          )
        )
      );
    }
  }]);
  return DatePicker;
}(React.PureComponent);

var birthStart = moment().subtract(118, 'year');
var birthEnd = moment().subtract(18, 'year');
DatePicker.propTypes = {
  start: PropTypes.object,
  end: PropTypes.object,
  theme: PropTypes.object
};
DatePicker.defaultProps = {
  start: birthStart.toDate(),
  end: birthEnd.toDate(),
  error: '',
  val: ''

};
var DatePicker$1 = onClickOutside(DatePicker);

// import React from 'react'
// import ReactDOM from 'react-dom'

// ReactDOM.render(<App />, document.getElementById('root'))

module.exports = DatePicker$1;
